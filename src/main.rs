extern crate smithay_client_toolkit as sctk;

use sctk::{
    default_environment,
    environment::SimpleGlobal,
    new_default_environment,
    output::{with_output_info, OutputInfo},
    reexports::{
        calloop,
        client::protocol::{wl_output, wl_shm, wl_surface},
        client::{Attached, Main},
        protocols::wlr::unstable::layer_shell::v1::client::{
            zwlr_layer_shell_v1, zwlr_layer_surface_v1,
        },
    },
    shm::DoubleMemPool,
    WaylandSource,
};
use std::{
    cell::{Cell, RefCell},
    io::{BufWriter, Seek, SeekFrom, Write},
    rc::Rc,
};

default_environment!(Environment,
    fields = [
        layer_shell: SimpleGlobal<zwlr_layer_shell_v1::ZwlrLayerShellV1>,
    ],
    singles = [
        zwlr_layer_shell_v1::ZwlrLayerShellV1 => layer_shell
    ],
);

const WIDTH: usize = 1920;
const HEIGHT: usize = 30;

#[derive(Debug, Copy, Clone)]
struct BufferDimensions {
    width: usize,
    height: usize,
    unpadded_bytes_per_row: usize,
    padded_bytes_per_row: usize,
}

impl BufferDimensions {
    fn new(width: usize, height: usize) -> Self {
        let bytes_per_pixel = std::mem::size_of::<u32>();
        let unpadded_bytes_per_row = width * bytes_per_pixel;
        let align = wgpu::COPY_BYTES_PER_ROW_ALIGNMENT as usize;
        let padded_bytes_per_row_padding = (align - unpadded_bytes_per_row % align) % align;
        let padded_bytes_per_row = unpadded_bytes_per_row + padded_bytes_per_row_padding;
        Self {
            width,
            height,
            unpadded_bytes_per_row,
            padded_bytes_per_row,
        }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub enum Event {
    Closed,
    Configure { width: u32, height: u32 },
}

async fn run() {
    let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);
    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::LowPower,
            compatible_surface: None,
        })
        .await
        .expect("failed to find an appropiate adapter");

    // Create the logical device and command queue.
    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                features: wgpu::Features::empty(),
                limits: wgpu::Limits::default(),
                shader_validation: true,
            },
            None,
        )
        .await
        .expect("failed to create device");

    let buffer_dimensions = BufferDimensions::new(WIDTH, HEIGHT);
    let output_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: None,
        size: (buffer_dimensions.padded_bytes_per_row * buffer_dimensions.height) as u64,
        usage: wgpu::BufferUsage::MAP_READ | wgpu::BufferUsage::COPY_DST,
        mapped_at_creation: false,
    });

    let texture_extent = wgpu::Extent3d {
        width: buffer_dimensions.width as u32,
        height: buffer_dimensions.height as u32,
        depth: 1,
    };

    // The render pipeline renders data into this texture
    let texture = device.create_texture(&wgpu::TextureDescriptor {
        size: texture_extent,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Rgba8UnormSrgb,
        usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::COPY_SRC,
        label: None,
    });

    let (environment, display, wl_queue) =
        new_default_environment!(Environment, fields = [layer_shell: SimpleGlobal::new(),])
            .expect("initial roundtrip failed");

    let surfaces = Rc::new(RefCell::new(Vec::new()));

    let layer_shell = environment.require_global::<zwlr_layer_shell_v1::ZwlrLayerShellV1>();

    let environment_handle = environment.clone();
    let surfaces_handle = Rc::clone(&surfaces);
    let output_handler = move |output: wl_output::WlOutput, info: &OutputInfo| {
        if info.obsolete {
            // An output has been removed, so release it.
            surfaces_handle.borrow_mut().retain(|(i, _)| *i != info.id);
            output.release();
        } else {
            // An output has been created, so construct a surface for it.
            let surface = environment_handle.create_surface().detach();
            let pools = environment_handle
                .create_double_pool(|_| {})
                .expect("failed to create a memory pool");
            (*surfaces_handle.borrow_mut()).push((
                info.id,
                create_surface(&output, surface, &layer_shell.clone(), pools),
            ));
        }
    };

    // Process currently existing outputs
    for output in environment.get_all_outputs() {
        if let Some(info) = with_output_info(&output, Clone::clone) {
            output_handler(output, &info);
        }
    }

    // Setup a listener for changes.
    // The listener will live for as long as we keep this handle alive.
    let _listener_handle =
        environment.listen_for_outputs(move |output, info, _| output_handler(output, info));

    let mut event_loop = calloop::EventLoop::<()>::new().unwrap();

    WaylandSource::new(wl_queue)
        .quick_insert(event_loop.handle())
        .unwrap();

    loop {
        let command_buffer = {
            let mut encoder =
                device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
            encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment: &texture.create_view(&wgpu::TextureViewDescriptor::default()),
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color::GREEN),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            // Copy the data from the texture to the buffer.
            encoder.copy_texture_to_buffer(
                wgpu::TextureCopyView {
                    texture: &texture,
                    mip_level: 0,
                    origin: wgpu::Origin3d::ZERO,
                },
                wgpu::BufferCopyView {
                    buffer: &output_buffer,
                    layout: wgpu::TextureDataLayout {
                        offset: 0,
                        bytes_per_row: buffer_dimensions.padded_bytes_per_row as u32,
                        rows_per_image: 0,
                    },
                },
                texture_extent,
            );

            encoder.finish()
        };

        queue.submit(Some(command_buffer));

        {
            let mut surfaces = surfaces.borrow_mut();
            let mut i = 0;
            while i != surfaces.len() {
                if {
                    let (surface, _layer_surface, next_render_event, pools) = &mut surfaces[i].1;
                    match next_render_event.take() {
                        Some(Event::Closed) => true,
                        Some(Event::Configure { .. }) => {
                            {
                                let pool = pools.pool().expect("failed to get a memory pool");

                                let width = WIDTH as i32;
                                let height = HEIGHT as i32;
                                let stride = 4 * width;

                                // Make sure the pool is the right size.
                                pool.resize((stride * height) as usize).unwrap();

                                // Create a new buffer from the pool.
                                let buffer =
                                    pool.buffer(0, width, height, stride, wl_shm::Format::Argb8888);

                                // Write the texture data to all bytes of the pool.
                                pool.seek(SeekFrom::Start(0)).unwrap();
                                {
                                    let mut writer = BufWriter::new(&mut *pool);

                                    {
                                        let buffer_slice = output_buffer.slice(..);
                                        let buffer_future =
                                            buffer_slice.map_async(wgpu::MapMode::Read);

                                        device.poll(wgpu::Maintain::Wait);

                                        if buffer_future.await.is_ok() {
                                            let padded_buffer = buffer_slice.get_mapped_range();

                                            for chunk in padded_buffer
                                                .chunks(buffer_dimensions.padded_bytes_per_row)
                                            {
                                                writer
                                                    .write(
                                                        &chunk[..buffer_dimensions
                                                            .unpadded_bytes_per_row],
                                                    )
                                                    .expect("failed to write chunk to output");
                                            }

                                            std::mem::drop(padded_buffer);

                                            output_buffer.unmap();
                                        }
                                    }

                                    writer.flush().expect("failed to flush graphics buffer");
                                }

                                // Attach the buffer to the surface and mark the entire surface as damaged
                                surface.attach(Some(&buffer), 0, 0);
                                surface.damage_buffer(0, 0, width, height);

                                // Finally, commit the surface
                                surface.commit();
                            }
                            false
                        }
                        None => false,
                    }
                } {
                    // If you turn the surface tuple into a struct, you should probably destroy these in a `Drop` implementation.
                    (surfaces[i].1).0.destroy();
                    (surfaces[i].1).1.destroy();

                    surfaces.remove(i);
                }
                i += 1;
            }
        }

        display.flush().unwrap();
        event_loop.dispatch(None, &mut ()).unwrap();
    }
}

/// Creates a surface to be used with Wayland. Returns a tuple because this was quickly hacked together.
/// And yes, it does return `pools` even though it doesn't even use it.
fn create_surface(
    output: &wl_output::WlOutput,
    surface: wl_surface::WlSurface,
    layer_shell: &Attached<zwlr_layer_shell_v1::ZwlrLayerShellV1>,
    pools: DoubleMemPool,
) -> (
    wl_surface::WlSurface,
    Main<zwlr_layer_surface_v1::ZwlrLayerSurfaceV1>,
    Rc<Cell<Option<Event>>>,
    DoubleMemPool,
) {
    let layer_surface = layer_shell.get_layer_surface(
        &surface,
        Some(&output),
        zwlr_layer_shell_v1::Layer::Overlay,
        "wgpu-sctk-combo".to_owned(),
    );

    layer_surface.set_size(WIDTH as u32, HEIGHT as u32);
    layer_surface.set_anchor(zwlr_layer_surface_v1::Anchor::Top);

    let next_render_event = Rc::new(Cell::new(None::<Event>));
    let next_render_event_handle = Rc::clone(&next_render_event);
    layer_surface.quick_assign(move |layer_surface, event, _| {
        match (event, next_render_event_handle.get()) {
            (zwlr_layer_surface_v1::Event::Closed, _) => {
                next_render_event_handle.set(Some(Event::Closed));
            }
            (
                zwlr_layer_surface_v1::Event::Configure {
                    serial,
                    width,
                    height,
                },
                next,
            ) if next != Some(Event::Closed) => {
                layer_surface.ack_configure(serial);
                next_render_event_handle.set(Some(Event::Configure { width, height }));
            }
            (_, _) => {}
        }
    });

    // Commit so that the server will send a configure event.
    surface.commit();

    (surface, layer_surface, next_render_event, pools)
}

fn main() {
    futures::executor::block_on(run());
}
