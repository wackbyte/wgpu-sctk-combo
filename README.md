# wgpu-sctk-combo

A combination of the layer example from Smithay's client toolkit and the capture example from wgpu-rs.
It's just a green clear color, though.

The code is really bad and hacked together, and this might not be how you're supposed to do this, but I was so surprised that I actually got it to work that I decided to put it here.

## License

[This repository is "licensed" under the Unlicense.](UNLICENSE)
